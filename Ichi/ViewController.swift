//
//  ViewController.swift
//  Ichi
//
//  Created by Jarosław Bąk on 02.09.2016.
//  Copyright © 2016 Jarosław Bąk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*let butek : UIButton = UIButton()
        butek.frame = CGRect(x: 50, y: 50, width: 100, height: 50)
        butek.setTitle("OK", forState: UIControlState.Normal)
        butek.backgroundColor = UIColor.blueColor()
        self.view.addSubview(butek)
        butek.addTarget(self, action: #selector(jazda), forControlEvents: UIControlEvents.TouchUpInside)
        butek.tag = 6666
        let sz = UIScreen.mainScreen().bounds
        print(sz)*/
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    /*func jazda (sender : UIButton){
        print(sender.tag)
    }*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*@IBAction func akcja(sender: AnyObject) {
        print("lel")
    }*/
    var PokeArray = [Int]()
    var firstMem = UIButton?()
    var widok = UIViewController()
    var trudnosc = 0;
    
    func buttonClicked(sender: UIButton?){
        //print("imagesy/bg\(Int((sender?.tag)!)).png")
        let triggerTime = (Int64(NSEC_PER_SEC) * 1)
        
        
        //show pokemon
        sender?.setBackgroundImage(UIImage(named: "imagesy/bg\(PokeArray[Int((sender?.tag)!)-1]).png"), forState: .Normal)
        if firstMem == nil {
            firstMem = sender
        } else { //check if sender has same img as previous card
            if sender != firstMem {
                widok.view.userInteractionEnabled = false
                if PokeArray[Int((sender?.tag)!)-1] == PokeArray[Int((firstMem?.tag)!)-1] {
//                    firstMem?.addTarget(self, action: nil, forControlEvents: .TouchUpInside)
                    //sender?.addTarget(self, action: nil, forControlEvents: .TouchUpInside)
                    firstMem?.enabled = false
                    sender?.enabled = false
                    widok.view.userInteractionEnabled = true
                    
                    //checking for win
                    if trudnosc != 0 {
                        trudnosc -= 1
                    } else {
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                            self.changeView()
                        })
                    }
//                    firstMem = nil
//                    return
                } else {
                    let pierwszy : UIButton? = firstMem
                    //firstMem?.setBackgroundImage(UIImage(named: "imagesy/bg0.png"), forState: .Normal)
                    //sender?.setBackgroundImage(UIImage(named: "imagesy/bg0.png"), forState: .Normal)
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                        self.pokeballs(forFirst: pierwszy,andSecond: sender)
                    })
//                    firstMem = nil
//                    return
                    
                }
                firstMem = nil
                return
                
            }
        }
        
        
        
    }
    
    func pokeballs(forFirst one: UIButton?, andSecond two: UIButton?){
        one?.setBackgroundImage(UIImage(named: "imagesy/bg0.png"), forState: .Normal)
        two?.setBackgroundImage(UIImage(named: "imagesy/bg0.png"), forState: .Normal)
        widok.view.userInteractionEnabled = true
    }
    
    func changeView() {
        /*let vc : UIViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("w1"))!
        self.showViewController(vc, sender: vc)*/
        navigationController?.popToRootViewControllerAnimated(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print(segue.identifier)
        widok = segue.destinationViewController as! ViewController2
        let target = segue.destinationViewController as! ViewController2
        var MemoryBtSize: CGFloat = 100
        let MemoryBtMargin: CGFloat = 8
        firstMem = nil
        
        
        
        switch segue.identifier {
        case "easy"?:
            trudnosc = 5
            PokeArray = [Int]()
            for i in 1...6 {
                PokeArray.append(i)
            }
            for i in 1...6 {
                PokeArray.append(i)
            }
            for idx in 0..<PokeArray.count {
                let rnd = Int(arc4random_uniform(UInt32(idx)))
                if rnd != idx {
                    swap(&PokeArray[idx], &PokeArray[rnd])
                }
            }
            
            MemoryBtSize = CGFloat((Int(UIScreen.mainScreen().bounds.width) / 4))
            MemoryBtSize -= 5*MemoryBtMargin/4
            for i in 0 ..< 3{
                for j in 0 ..< 4{
                    let bt : UIButton = UIButton()
                    let iks = MemoryBtMargin + CGFloat(j * Int(MemoryBtMargin)) + CGFloat(j * Int(MemoryBtSize))
                    let igrek = 225 + CGFloat(i * Int(MemoryBtMargin)) + CGFloat(i * Int(MemoryBtSize))
                    print(iks)
                    print(igrek)
                    
                    bt.frame = CGRect(x: iks, y: igrek, width: MemoryBtSize, height: MemoryBtSize)
                    //bt.backgroundColor = UIColor.blueColor()
                    bt.setBackgroundImage(UIImage(named: "imagesy/bg0.png"), forState: .Normal)
                    bt.tag = (i*4)+j+1
                    target.view.addSubview(bt)
                    
                    //click
                    bt.addTarget(self, action: #selector(buttonClicked), forControlEvents: .TouchUpInside)
                    
                    
                    
                }
            }
            break
        case "normal"?:
            trudnosc = 11
            PokeArray = [Int]()
            for i in 1...12 {
                PokeArray.append(i)
            }
            for i in 1...12 {
                PokeArray.append(i)
            }
            for idx in 0..<PokeArray.count {
                let rnd = Int(arc4random_uniform(UInt32(idx)))
                if rnd != idx {
                    swap(&PokeArray[idx], &PokeArray[rnd])
                }
            }
            
            MemoryBtSize = CGFloat((Int(UIScreen.mainScreen().bounds.width) / 4))
            MemoryBtSize -= 5*MemoryBtMargin/4
            for i in 0 ..< 6{
                for j in 0 ..< 4{
                    let bt : UIButton = UIButton()
                    let iks = MemoryBtMargin + CGFloat(j * Int(MemoryBtMargin)) + CGFloat(j * Int(MemoryBtSize))
                    let igrek = 91.5 + CGFloat(i * Int(MemoryBtMargin)) + CGFloat(i * Int(MemoryBtSize))
//                    print(iks)
//                    print(igrek)
                    
                    bt.frame = CGRect(x: iks, y: igrek, width: MemoryBtSize, height: MemoryBtSize)
                    //bt.backgroundColor = UIColor.blueColor()
                    bt.setBackgroundImage(UIImage(named: "imagesy/bg0.png"), forState: .Normal)
                    bt.tag = (i*4)+j+1
                    target.view.addSubview(bt)
                    
                    //click
                    bt.addTarget(self, action: #selector(buttonClicked), forControlEvents: .TouchUpInside)
                    
                }
            }
            break
        default:
            break
        }
        
    }


}

